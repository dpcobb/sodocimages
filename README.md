# SODocImages

SODocImages is a Python CLI tool to help resize and rename images for the SODoc engine.

* [Requirements](#makdown-header-requirements)
* [Installing](#markdown-header-installing)
* [Usage](#markdown-header-using-the-resizer)

## Requirements

SODocImages requires Python version 2.7.12 or higher and PIL/PILLOW module for images.

## Installing

First ensure you have Python and PIL/PILLOW installed on your system. Then download the repository, or just main.py to the location of your choice.

## Using The Resizer

Navigate the location you saved the resizer in your Command Line and run the script. The script will ask you to enter an option either 1 or 2. Option 1 will change a single image, while option 2 will change an entire directory of images. The next question will be for the paths of the image/directory for both input and output. Next the the script will ask for the Doc section title and filename without the .md. The section name should be as it appears, ex 'Getting Started', the formatter will format the section name. If you are formatting one image it will ask for an image number to append to the file name, if you are formatting a directory a number will be assigned as the image is formatted.

### Resizing a Directory

If you are resizing a directory make sure, to avoid errors, the directory only contains images or images and an output folder. Other file types may cause errors.

### Output

The resizer will output images scaled down to 640px wide with the proper naming convention set forth in the SODoc Engine documentation.
