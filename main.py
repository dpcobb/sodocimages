#!/usr/bin/python
"""
SODocImages - DCobb 10/2018
Resizes and renames images for SODoc
"""
from PIL import Image
import os, sys

class ImageResizer:
    def __init__(self):
        self.orginalPath = ''
        self.outputPath = ''
        self.option = ''
        self.section = ''
        self.file = ''
        self.imageNum = ''
        self.width = 640

    def resizeSingleImage(self):
        img = Image.open(self.originalPath)
        wPer = (self.width / float(img.size[0]))
        hSize = int((float(img.size[1]) * float(wPer)))
        img = img.resize((self.width, hSize), Image.ANTIALIAS)
        output = self.outputPath + self.section.replace(' ', '-').lower() + '_' + self.file.lower() + '_' + self.imageNum + '.png'
        img.save(output)
        print 'File created: ' + output

    def resizeFolder(self):
        i = 0
        for item in os.listdir(self.originalPath):
            if os.path.isfile(self.originalPath + item):
                img = Image.open(self.originalPath + item)
                wPer = (self.width / float(img.size[0]))
                hSize = int((float(img.size[1]) * float(wPer)))
                img = img.resize((self.width, hSize), Image.ANTIALIAS)
                output = self.outputPath + self.section.replace(' ', '-').lower() + '_' + self.file.lower() + '_' + str(i) + '.png'
                img.save(output)
                print 'File created: ' + output
                i += 1


    def run(self):
        print '**********\n SODoc Image Formatter \n**********'
        print 'Enter 1 to edit a single image'
        print 'Enter 2 to edit a directory of images'
        self.option = raw_input('Please select an option...: ')
        if self.option == '1':
            self.originalPath = raw_input('Enter the original image path: ')
            self.outputPath = raw_input('Enter the output path: ')
            self.section = raw_input('Enter the section the image appears in: ')
            self.file = raw_input('Enter the file the image appears in: ')
            self.imageNum = raw_input('Enter enter the image number: ')
            self.resizeSingleImage()
        elif self.option == '2':
            self.originalPath = raw_input('Enter the directory path: ')
            self.outputPath = raw_input('Enter the output path: ')
            self.section = raw_input('Enter the section the images appear in: ')
            self.file = raw_input('Enter the file the images appear in: ')
            self.resizeFolder()

launch = ImageResizer()
launch.run();
